package com.example.aep_project.pedometerapplication;

/**
 * Created by AEP-Project on 9/28/2014.
 * In this class is stored the type of the Data used to store the
 * users information.
 */
public class Data {
    // Labels table name
    public static final String TABLE = "Data";

    // Labels Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_DATE = "date";
    public static final String KEY_STEP = "step";
    public static final String KEY_DISTANCE = "distance";
    public static final String KEY_CALORIE = "calorie";

    // property helping to keep data
    public int data_ID;
    public String date;
    public int step;
    public double distance;
    public double calorie;
}