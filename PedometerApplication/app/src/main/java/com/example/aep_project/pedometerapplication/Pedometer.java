package com.example.aep_project.pedometerapplication;

/*The class uses Accelerometer sensor to count the steps of the user.
*It calculates the distance the user has voyaged
* by the steps' length that it is set in a see-kbar proposed to the user
 */
import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.view.View;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.content.Context;

import java.text.DecimalFormat;

public class Pedometer extends Activity implements View.OnClickListener {
    //needed for this class
    TextView textView,textView2,textView3,textView4,textView5,textView6;
    ImageButton buttonReset;
    private MediaPlayer sound;

    SensorManager sensorManager;
    float acceleration,x,y,z,g;
    int numSteps,StepsDone,bar;
    SeekBar seekBar;
    double threshold;

    double dist,stepl,calori,weight;
    //needed for the other activities the class calls
    ImageButton btnAdd;
    ImageButton btnGetAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedometer);

        textView=(TextView)findViewById(R.id.textView);
        textView2=(TextView)findViewById(R.id.textView2);
        textView3=(TextView)findViewById(R.id.textView3);

        textView4=(TextView)findViewById(R.id.textView4);
        textView5=(TextView)findViewById(R.id.textView5);
        textView6=(TextView)findViewById(R.id.textView6);

        buttonReset=(ImageButton)findViewById(R.id.button);
        seekBar=(SeekBar)findViewById(R.id.seekBar);
        sound =  MediaPlayer.create(Pedometer.this,R.raw.shortbeep);

        seekBar.setProgress(2);
        seekBar.setOnSeekBarChangeListener(seekBarListener);
        threshold=2.12;
        textView5.setText(String.valueOf(threshold));

        numSteps=0;

        acceleration=0.00f;
        enableAccelerometerListening();

        Intent intent = getIntent();
        weight = intent.getDoubleExtra("weight" , 0);
        weight = 2.20462262  * weight;

        //for the calling activities
        btnAdd = (ImageButton) findViewById(R.id.btnAdd);
        btnGetAll = (ImageButton) findViewById(R.id.btnGetAll);

        btnAdd.setOnClickListener(this);
        btnGetAll.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pedometer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void enableAccelerometerListening() {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(sensorEventListener,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    private SensorEventListener sensorEventListener=new SensorEventListener() {



        @Override
        public void onSensorChanged(SensorEvent event) {
            DecimalFormat f = new DecimalFormat("##.##");


            x=event.values[0];
            y=event.values[1];
            z=event.values[2];


        g = ((x * x + y * y + z * z)) / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);

            if( g>threshold){

                numSteps++;
                textView4.setText( String.valueOf(numSteps));
                sound.start();
            }

            textView.setText("X = "+ f.format(x));
            textView2.setText("Y = "+ f.format(y));
            textView3.setText("Z = " + f.format(z));


        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    public void resetSteps(View v){
        DecimalFormat f = new DecimalFormat("#######.####");

        StepsDone=numSteps;
        numSteps=0;
        textView4.setText(String.valueOf(numSteps));

        if(threshold==1.35) {
            stepl = 0.0003605;
            dist = StepsDone * stepl;
            calori = 0.5 * weight * dist;
        }
        else {
            if (threshold == 1.72) {
                stepl = 0.0007332;
                dist = StepsDone * stepl;
                calori = 0.5 * weight * dist;
            }
            else{
                if (threshold == 2.12) {
                    stepl = 0.001044;
                    dist = StepsDone * stepl;
                    calori = 0.5 * weight * dist;
                }
                else{
                    if (threshold == 2.52) {
                        stepl = 0.001218;
                        dist = StepsDone * stepl;
                        calori = 0.5 * weight * dist;
                    }
                    else {
                        stepl = 0.001553;
                        dist = StepsDone * stepl;
                        calori = 0.5 * weight * dist;
                    }
                }

            }

            textView6.setText(f.format(dist)+" miles   "+ f.format(calori)+" calories  " );
        }
    }

    private OnSeekBarChangeListener seekBarListener = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            bar = seekBar.getProgress();
            switch (bar) {
                case 0:
                    threshold = 1.35;
                    textView5.setText(String.valueOf(threshold));
                    break;
                case 1:
                    threshold = 1.72;
                    textView5.setText(String.valueOf(threshold));
                    break;
                case 2:
                    threshold = 2.12;
                    textView5.setText(String.valueOf(threshold));
                    break;
                case 3:
                    threshold = 2.52;
                    textView5.setText(String.valueOf(threshold));
                    break;
                case 4:
                    threshold = 2.92;
                    textView5.setText(String.valueOf(threshold));
                    break;
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.btnAdd):
                Intent intent1 = new Intent(this, DataDetail.class);
                intent1.putExtra("data_Id", 0);
                intent1.putExtra("steps", StepsDone);
                intent1.putExtra("distance" ,dist);
                intent1.putExtra("calorie", calori);
                startActivity(intent1);
                break;

            case (R.id.btnGetAll):
                Intent intent2 = new Intent(this, DataList.class);
                startActivity(intent2);
                break;
        }
    }
}
