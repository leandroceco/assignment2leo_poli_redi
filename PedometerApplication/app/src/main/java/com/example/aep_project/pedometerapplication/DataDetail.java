package com.example.aep_project.pedometerapplication;

/*This class is used to present information that the user wants to save
*on the database. The user can chage some of the information that
* is given from the Pedometer class
*/
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class DataDetail extends ActionBarActivity implements android.view.View.OnClickListener{

    ImageButton btnSave;
    ImageButton btnDelete;
    ImageButton btnClose;
    EditText editDate;
    EditText editStep;
    EditText editCalorie;
    EditText editDistance;
    private int _data_Id=0;
    double calorie, distance;
    int step;
    Date date ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_detail);

        btnSave = (ImageButton) findViewById(R.id.btnSave);
        btnDelete = (ImageButton) findViewById(R.id.btnDelete);
        btnClose = (ImageButton) findViewById(R.id.btnClose);

        editDate = (EditText) findViewById(R.id.editDate);
        editStep = (EditText) findViewById(R.id.editStep);
        editCalorie = (EditText) findViewById(R.id.editCalorie);
        editDistance = (EditText) findViewById(R.id.editDistance);

        btnSave.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnClose.setOnClickListener(this);

        GregorianCalendar c = new GregorianCalendar();
        date = c.getTime();

        _data_Id =0;
        Intent intent = getIntent();

        DecimalFormat f = new DecimalFormat("#######.####");
        step = intent.getIntExtra("steps", 0);
        calorie = intent.getDoubleExtra("calorie", 0);
        distance = intent.getDoubleExtra("distance", 0);


        editCalorie.setText(f.format(calorie));
        editDate.setText(String.valueOf(date));
        editStep.setText(f.format(step));
        editDistance.setText(f.format(distance));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.data_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.btnSave)){
            DataRepo repo = new DataRepo(this);
            Data data = new Data();
            data.calorie = calorie;
            data.step = step;
            data.date = editDate.getText().toString();
            data.distance = distance;
            data.data_ID=_data_Id;

            if (_data_Id==0){
                _data_Id = repo.insert(data);

                Toast.makeText(this,"New Record Insert",Toast.LENGTH_SHORT).show();
            }else{
                repo.update(data);
                Toast.makeText(this,"Record updated",Toast.LENGTH_SHORT).show();
            }
        }else if (view== findViewById(R.id.btnDelete)){
            DataRepo repo = new DataRepo(this);
            repo.delete(_data_Id);
            Toast.makeText(this, "Record Deleted", Toast.LENGTH_SHORT).show();
            finish();
        }else if (view== findViewById(R.id.btnClose)){
            finish();
        }
    }
}
