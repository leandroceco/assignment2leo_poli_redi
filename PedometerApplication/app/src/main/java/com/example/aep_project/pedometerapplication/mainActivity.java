package com.example.aep_project.pedometerapplication;

/*Presents the user to the widgets proposed.
*They can choose from map, compass and pedometer activity
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


public class mainActivity extends ActionBarActivity implements View.OnClickListener{

    ImageButton pedometer ;
    ImageButton map;
    ImageButton compass;

    double weight ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pedometer = (ImageButton)findViewById(R.id.pedometer);
        map = (ImageButton)findViewById(R.id.map);
        compass = (ImageButton)findViewById(R.id.compass);

        pedometer.setOnClickListener(this);
        map.setOnClickListener(this);
        compass.setOnClickListener(this);

        getWeight();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case (R.id.pedometer) :
                startPedometer();
                break;
            case (R.id.map) :
                startMap();
                break;
            case(R.id.compass):
                startCompass();
                break;
        }
    }

    public void startPedometer () {
        Intent intent = new Intent(this, Pedometer.class) ;
        intent.putExtra("weight" , weight);
        startActivity(intent);
    }

    public void startMap(){
        Intent intent = new Intent(this, MCActivity.class) ;
        startActivity(intent);
    }

    public void startCompass() {
        Intent intent = new Intent(this, CompassActivity.class);
        startActivity(intent);
    }

    public void getWeight() {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Hello!");
            alert.setMessage("Put your weight:");

            // Create EditText for entry
            final EditText input = new EditText(this);
            input.setInputType(2);
            alert.setView(input);

            // Make an "OK" button to save the name
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {

                    // Grab the EditText's input
                    weight = Float.parseFloat(input.getText().toString());

                    // Welcome the new user
                    Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_LONG).show();
                }
            });
            alert.show();
        }

    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Application")
                .setMessage("Are you sure you want to close this application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    }







